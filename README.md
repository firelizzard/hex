# Hex

```
Hex encode or decode from stdin

Usage:
  hex [flags]

Flags:
  -d, --decode   Decode
  -h, --help     help for hex
```