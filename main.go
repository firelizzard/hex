package main

import (
	"encoding/hex"
	"errors"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var cmd = &cobra.Command{
	Use:   "hex",
	Short: "Hex encode or decode from stdin",
	Run: func(cmd *cobra.Command, args []string) {
		if flag.Decode {
			decode()
		} else {
			encode()
		}
	},
}

var flag = struct {
	Decode bool
}{}

const N = 20

func main() {
	cmd.Flags().BoolVarP(&flag.Decode, "decode", "d", false, "Decode")
	_ = cmd.Execute()
}

func encode() {
	var in [2 << N]byte
	var out [1 << N]byte

	for {
		n, err := os.Stdin.Read(in[:])
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		n = hex.Encode(out[:], in[:n])

		_, err = os.Stdout.Write(out[:n])
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
	}

	println()
}

func decode() {
	var in [1 << N]byte
	var out [2 << N]byte
	var last int

	for {
		n, err := os.Stdin.Read(in[:])
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		if n > 0 && in[n-1] == '\n' {
			n--
		}

		n, err = hex.Decode(out[:], in[:n])
		if err != nil {
			log.Fatal(err)
		}
		last = n

		_, err = os.Stdout.Write(out[:n])
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
	}

	if last > 0 && out[last-1] != '\n' {
		println()
	}
}
